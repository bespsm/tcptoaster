#ifndef INCLUDE_CLASS_POINTERS_H
#define INCLUDE_CLASS_POINTERS_H
#include <memory>

#define DEFINE_CLASS_POINTERS(ClassName)    \
    typedef std::unique_ptr<ClassName> UniquePtr; \
    typedef std::shared_ptr<ClassName> SharedPtr; \
    typedef std::weak_ptr<ClassName> WeakPtr;

#endif // INCLUDE_CLASS_POINTERS_H
