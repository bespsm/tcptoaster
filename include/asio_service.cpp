#include "include/asio_service.h"
#include "include/service_manager.h"
#include "include/parameters_service.h"
#include <thread>
#include <iostream>
#include <boost/asio.hpp>

namespace {
class AsioService:
    public common::AsioService_IF{

public:
    AsioService():
        work_(service_){}

    void initialize(common::ServiceManager_IF& manager, common::ServiceParameters& parameters) override{
        param_threads = stoi(common::searchConfigParameter(parameters,"threads_number","2"));
        for (int i = 0; i < param_threads; ++i)
            threads_.emplace_back([this](){
                std::cout << "thread started: " << std::this_thread::get_id() << '\n';
                service_.run();
            });
    }

    void stop() override {
      service_.stop();
      for (std::thread& thread: threads_){
          std::cout << "thread stop: " << thread.get_id() << '\n';
          thread.join();
      }
      threads_.clear();
    }

    boost::asio::io_service& getIoService() override {
        return service_;
    }

private:
    int param_threads;
    boost::asio::io_service service_;
    boost::asio::io_service::work work_;
    std::vector<std::thread> threads_;
};
} //namespace

namespace common {

const ServiceId AsioServiceId = "asio";

BaseService_IF::UniquePtr AsioFactory::CreateService()
{
    return common::BaseService_IF::UniquePtr(new AsioService());
}

} //common


