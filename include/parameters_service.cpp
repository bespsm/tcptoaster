#include <boost/filesystem.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include "include/parameters_service.h"
#include "include/parameters.h"
#include "include/base_service.h"
#include <iostream>

using boost::property_tree::ptree;

namespace
{
  struct ServiceConfiguration;

  common::ParametersGroup GetGroup(const std::string& name, const ptree& groupTree)
  {
    common::ParametersGroup group(name);

    if (groupTree.empty())
    {
      return group;
    }

    for(const ptree::value_type& child : groupTree)
    {
      if (child.second.empty())
      {
        group.Parameters.push_back(common::Parameter(child.first, child.second.data()));
        continue;
      }
      group.Groups.push_back(GetGroup(child.first, child.second));
    }

    return group;
  }

  void AddParameter(common::ServiceParameters& params, const std::string& name, const ptree& tree)
  {
    if (tree.empty())
    {
      params.Parameters.push_back(common::Parameter(name, tree.data()));
      return;
    }
    params.Groups.push_back(GetGroup(name, tree));
  }

} // namespace


common::Configuration common::parseConfiguration(const std::string& configPath)
{
  ptree pt;
  read_xml(configPath, pt);
  Configuration configuration;
  for(const ptree::value_type& config: pt)
  {
    if (config.first != "config")
    {
      std::cerr << "Unknown root tag " << config.first << " in the config file '" << configPath << "'" << std::endl;
      continue;
    }

    for(const ptree::value_type& param: config.second)
    {
      if (param.first != "services")
      {
        AddParameter(configuration.Parameters, param.first, param.second);
        continue;
      }

      for(const ptree::value_type& service: param.second)
      {
        if (service.first != "service")
        {
          std::cerr << "Unknown tag " << service.first << " inside 'service' in the config file '" << configPath << "'" << std::endl;
          continue;
        }

        common::ServiceConfiguration serviceConfig;
        serviceConfig.Id = service.second.get<std::string>("id");
        //serviceConfig.Path = service.second.get<std::string>("path");
        if (boost::optional<const ptree&> dependsOn = service.second.get_child_optional("depends_on"))
        {
          for(const ptree::value_type& depend: dependsOn.get())
          {
            if (depend.first != "id")
            {
              continue;
            }
            serviceConfig.Dependencies.push_back(depend.second.data());
          }
        }

        if (boost::optional<const ptree&> parameters = service.second.get_child_optional("parameters"))
        {
          for(const ptree::value_type& parameter: parameters.get())
          {
            AddParameter(serviceConfig.Parameters, parameter.first, parameter.second);
          }
        }

        configuration.Services.push_back(serviceConfig);
      }
    }
  }
  return configuration;
}

common::Configuration common::parseConfigurationDirectory(const std::string& directory)
{
  using namespace boost::filesystem;
  common::Configuration configuration;
  path p(directory);
  directory_iterator dirIt(p);
  directory_iterator end;
   if (!is_directory(p)) {return configuration;}
  std::for_each(dirIt, end, [&configuration](const directory_entry& entry)
  //for (auto&& entry : directory_iterator(p))
  {
    if  (entry.path().filename().extension() == ".conf")
    {
      std::cout << "Parsing config file: " << entry.path().native() << std::endl;
      common::Configuration tmp = common::parseConfiguration(entry.path().string());
      configuration.Services.insert(configuration.Services.end(), tmp.Services.begin(), tmp.Services.end());
      configuration.Parameters.Groups.insert(configuration.Parameters.Groups.end(), tmp.Parameters.Groups.begin(), tmp.Parameters.Groups.end());
      configuration.Parameters.Parameters.insert(configuration.Parameters.Parameters.end(), tmp.Parameters.Parameters.begin(), tmp.Parameters.Parameters.end());
    }
  }
  );
  return configuration;
}


namespace
{
  void AddDependencies(ptree& serviceTree, const std::vector<common::ServiceId>& ids)
  {
    if (ids.empty())
    {
      return;
    }
    ptree& deps = serviceTree.add("depends_on", "");
    for (auto idIt = ids.begin(); idIt != ids.end(); ++idIt)
    {
      deps.add("id", *idIt);
    }
  }

  void AddGroup(ptree& serviceTree, const common::ParametersGroup& group)
  {
    ptree& groupTree = serviceTree.add(group.Name, "");
    for (const common::Parameter& param : group.Parameters)
    {
      groupTree.add(param.Name, param.Value);
    }
    for (const common::ParametersGroup& grp: group.Groups)
    {
      AddGroup(groupTree, grp);
    }
  }

  void AddParameters(ptree& serviceTree, const common::ServiceParameters& params, const char* groupName)
  {
    if (params.Parameters.empty() && params.Groups.empty())
    {
      return;
    }

    ptree& paramsTree = serviceTree.add(groupName, "");
    for (const common::Parameter& param : params.Parameters)
    {
      paramsTree.add(param.Name, param.Value);
    }
    for (const common::ParametersGroup& grp: params.Groups)
    {
      AddGroup(paramsTree, grp);
    }
  }
}

void common::saveConfiguration(const common::ServicesConfiguration& services, const std::string& configPath)
{
  ptree pt;
  ptree& servicesPt = pt.put("config.services", "");

  for (const common::ServiceConfiguration& config: services)
  {
    ptree& serviceTree = servicesPt.add("service", "");
    serviceTree.add("id", config.Id);
    //serviceTree.add("path", config.Path);
    AddDependencies(serviceTree, config.Dependencies);
    AddParameters(serviceTree, config.Parameters, "parameters");
  }

  write_xml(configPath, pt);
}


namespace common {

std::string searchConfigGroupParameter(common::ServiceParameters& parameters,const std::string groupName,const std::string parameterName,const std::string defaultValue){

    auto group = std::find(parameters.Groups.begin(),parameters.Groups.end(),common::ParametersGroup(groupName));
    if (group == parameters.Groups.end()) return std::string(defaultValue);
    auto param = std::find((*group).Parameters.begin(),(*group).Parameters.end(),Parameter(parameterName,""));
    return param == (*group).Parameters.end() ? std::string(defaultValue) : (*param).Value;
}

std::string searchConfigParameter(common::ServiceParameters& parameters,const std::string parameterName,const std::string defaultValue){
    auto param = std::find(parameters.Parameters.begin(),parameters.Parameters.end(),common::Parameter(parameterName,""));
    return param == parameters.Parameters.end() ? std::string(defaultValue) : (*param).Value;
}

}

