#ifndef INCLUDE_ASIO_SERVICE_IF_H
#define INCLUDE_ASIO_SERVICE_IF_H

#include "include/base_service.h"
#include "include/class_pointers.h"

namespace boost {
namespace asio {
class io_service;
}
}

namespace common {
typedef std::string ServiceId;
extern const ServiceId AsioServiceId;

class AsioService_IF:
    public common::BaseService_IF{
public:
    DEFINE_CLASS_POINTERS(AsioService_IF);
    virtual boost::asio::io_service& getIoService() = 0;
    //virtual void initialize(ServiceManager_IF& manager, const ServiceParameters& parameters) = 0;
    //virtual void stop() = 0;
};


class AsioFactory : public BaseServiceFactory_IF  {
public:
    virtual common::BaseService_IF::UniquePtr CreateService() override;
};



} //common


#endif /* INCLUDE_ASIO_SERVICE_IF_H */
