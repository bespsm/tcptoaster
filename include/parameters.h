#ifndef INCLUDE_PARAMETERS_H
#define INCLUDE_PARAMETERS_H

#include <vector>
#include <string>

namespace common {

struct Parameter{
  std::string Name;
  std::string Value;
  Parameter(){}
  Parameter(const std::string& name, const std::string& value)
    : Name(name)
    ,Value(value){}

  bool operator==(const Parameter & equal){
      return Name == equal.Name;
  }
};

struct ParametersGroup{
  std::string Name;
  std::vector<Parameter> Parameters;
  std::vector<ParametersGroup> Groups;
  ParametersGroup(){}
  ParametersGroup(const std::string& name):
      Name(name){}

  bool operator==(const ParametersGroup & equal){
      return Name == equal.Name;
  }

};

typedef std::vector<ParametersGroup> ArrayGroups;
typedef std::vector<Parameter> ArrayParameters;

struct ServiceParameters{
  std::vector<ParametersGroup> Groups;
  std::vector<Parameter> Parameters;
};

} //common

#endif /* INCLUDE_PARAMETERS_H */
