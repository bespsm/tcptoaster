#ifndef INCLUDE_PARAMETERS_SERVICE_H
#define INCLUDE_PARAMETERS_SERVICE_H

#include "include/parameters.h"

namespace common
{
  typedef std::string ServiceId;
    struct ServiceConfiguration
  {
    ServiceId Id;
    std::vector<ServiceId> Dependencies;
    //std::string Path;
    ServiceParameters Parameters;
  };


  typedef std::vector<common::ServiceConfiguration> ServicesConfiguration;

  struct Configuration
  {
    common::ServiceParameters Parameters;
    std::vector<ServiceConfiguration> Services;
  };

  Configuration parseConfiguration(const std::string& configPath);
  void saveConfiguration(const ServicesConfiguration& configuration, const std::string& configPath);
  Configuration parseConfigurationDirectory(const std::string& directory);


  std::string searchConfigGroupParameter(
           common::ServiceParameters& parameters,
           const std::string groupName,
           const std::string parameterName,
           const std::string defaultValue);

  std::string searchConfigParameter(
           common::ServiceParameters& parameters,
           const std::string parameterName,
           const std::string defaultValue);
}

#endif // INCLUDE_PARAMETERS_SERVICE_H
