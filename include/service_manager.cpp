#include "include/base_service.h"
#include "include/service_manager.h"
#include <map>
#include <iostream>
#include <algorithm>
#include <stdexcept>

namespace {



struct ServiceData{
    common::ServiceId Id;
    std::vector<common::ServiceId> Dependencies;
    common::BaseServiceFactory_IF::SharedPtr Factory;
    common::ServiceParameters Parameters;
    common::BaseService_IF::SharedPtr Instance;
    ServiceData(common::ServiceInfromation info):
        Id(info.Id),Dependencies(info.Dependencies),
        Factory(info.Factory),Parameters(info.Parameters){}
};

bool IsServiceNotStarted(const std::pair<common::ServiceId, ServiceData>& data){
  return data.second.Instance == common::BaseService_IF::SharedPtr();
}


class ServiceManager : public common::ServiceManager_IF  {

typedef std::map<common::ServiceId,ServiceData> ServicesList;

private:
   bool isWorking = false;
   ServicesList services_;

bool isServiceRegistered(const common::ServiceId id) const {
    return services_.find(id) != services_.end();
}

bool isServiceInitialized(const common::ServiceId id) const {
    if(!(services_.find(id)->second.Instance)) return false;
    return true;
}

bool isServiceStarted(const ServiceData& data) const {
  return static_cast<bool>(data.Instance);
}

bool IsAllServicesStarted(const std::vector<common::ServiceId>& ids) const{
    for (std::vector<common::ServiceId>::const_iterator it = ids.begin(); it != ids.end(); ++it){
        const ServicesList::const_iterator servIt = services_.find(*it);
        if (servIt == services_.end()){
            std::cerr << "Service dependencies not resolved. " << *it << " not started\n";
            //throw std::runtime_error("Service dependencies not resolved");
        }
        else if (!isServiceStarted(servIt->second)){
            return false;
        }
    }
    return true;
}

ServiceData* GetNextServiceDataForStart(){
  for (ServicesList::iterator it = services_.begin(); it != services_.end(); ++it){
    if (!isServiceStarted(it->second) && IsAllServicesStarted(it->second.Dependencies)){
      return &it->second;
    }
  }
  return 0;
}

ServiceData* GetNextServiceDataForStop(){
    for (ServicesList::iterator it = services_.begin(); it != services_.end(); ++it){
        if (isServiceStarted(it->second) && IsAllDependentServicesStopped(it->first)){
          return &it->second;
        }
    }
    return 0;
}

void EnsureAllServicesStarted() const {
    ServicesList::const_iterator servIt = std::find_if(services_.begin(), services_.end(), IsServiceNotStarted);
    if (!services_.empty() && servIt != services_.end()){
        std::cerr << "not all services started\n";
    }
}

bool IsAllDependentServicesStopped(const common::ServiceId id) const {
    for (const ServicesList::value_type& serviceIt : services_){
        // Skip alreay sopped addons.
        if (!isServiceStarted(serviceIt.second)){
            continue;
        }
        // If current addon depends on passed.
        const std::vector<common::ServiceId>& deps = serviceIt.second.Dependencies;
        if (std::find(deps.begin(), deps.end(), id) != deps.end()){
            return false;
        }
    }
    return true;
}

public:

virtual ~ServiceManager() {
    try{
        if(isWorking)
            stop();
    }
    catch (...)
    {
    std::cerr << "unknown exception" << std::endl;
    throw;
    }
}

virtual void addService(const common::ServiceInfromation& service) override{
    if (!isServiceRegistered(service.Id)){
        std::cout << "service '" << service.Id << "' registered\n";
        services_.insert(std::make_pair(service.Id, ServiceData(service)));
    }
    else {
        std::cerr << "service '" << service.Id << "' already registered\n";
    }
}

virtual common::BaseService_IF::SharedPtr getService(const common::ServiceId id) override {
    if (!isServiceRegistered(id)){
        std::cerr << "service '" << id <<"' not registered\n";
        //throw std::runtime_error("trying get not registered service");
    }
    if (!isServiceInitialized(id)){
        std::cerr << "Service '" << id << "' not initialized\n";
        //throw std::runtime_error("service not initialized");
    }
    return services_.find(id)->second.Instance;
}

virtual void start() override {
    if(isWorking)
        std::cerr << "manager already started\n";
        //throw std::runtime_error("manager already started");
    if (!doStart()){
        std::cerr << "error on starting services\n";
        stop();
        //throw std::runtime_error("error on starting services");
    }
    isWorking = true;
}

private:
bool doStart() {
    std::cout << "Startting Application...\n";
    while (ServiceData* serviceData = GetNextServiceDataForStart()){
    common::BaseService_IF::SharedPtr service = serviceData->Factory->CreateService();
    try {
        service->initialize(*this, serviceData->Parameters);
        std::cout << "Service '" << serviceData->Id << "' successfully initialized." <<  std::endl;
    }
    catch (const std::exception& exc){
        std::cerr << "Failed to initialize service '" << serviceData->Id << "': "<< exc.what() <<  std::endl;
        return false;
    }
    serviceData->Instance = service;
    }
    EnsureAllServicesStarted();
    return true;
}

public:
virtual void stop() override{
    if(!isWorking)
        throw std::runtime_error("manager already stopped");
    doStop();
    isWorking = false;
}

private:
virtual void doStop() {
    std::cout << "Stopping Application...\n";
    if (services_.empty())
        return;
    while (ServiceData* serviceData = GetNextServiceDataForStop()){
    try{
        std::cout << "Stopping service '" << serviceData->Id << "'" <<  std::endl;
        serviceData->Instance->stop();
        serviceData->Instance.reset();
        std::cout << "Service '" << serviceData->Id << "' successfully stopped." <<  std::endl;
    }
    catch (const std::exception& exc){
        std::cerr << "Failed to stop service '" << serviceData->Id << "': "<< exc.what() <<  std::endl;
    }
    }
    //services_.clear();
}
};

} //namespace

namespace common {
ServiceManager_IF::UniquePtr CreateServiceManager(){
    return ServiceManager_IF::UniquePtr(new ServiceManager);
}
}
