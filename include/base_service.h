#ifndef INCLUDE_BASE_SERVICE_IF_H
#define INCLUDE_BASE_SERVICE_IF_H

#include "include/class_pointers.h"
#include "include/interface.h"


namespace common {

class ServiceManager_IF;
struct ServiceParameters;

class BaseService_IF : private common::Interface  {
public:
    DEFINE_CLASS_POINTERS(BaseService_IF);
    virtual void initialize(ServiceManager_IF& manager,ServiceParameters& parameters) = 0;
    virtual void stop() = 0;
};

class BaseServiceFactory_IF : private common::Interface  {
public:
    DEFINE_CLASS_POINTERS(BaseServiceFactory_IF);
    virtual common::BaseService_IF::UniquePtr CreateService() = 0;
};


} //common

#endif /* INCLUDE_BASE_SERVICE_IF_H */
