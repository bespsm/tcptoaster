#ifndef INCLUDE_BASE_INTERFACE_H
#define INCLUDE_BASE_INTERFACE_H

namespace common {

  class Interface
  {
  protected:
    Interface(){}
    virtual ~Interface(){}

    Interface(const Interface&) = delete;
    const Interface& operator=(const Interface&) = delete;
  };

}

#endif // INCLUDE_BASE_INTERFACE_H

