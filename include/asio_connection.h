#ifndef INCLUDE_ASIO_CONNECTION_IF_H
#define INCLUDE_ASIO_CONNECTION_IF_H

//#include "echo/echo_types.h"
//#include "include/class_pointers.h"
#include "include/interface.h"
#include "include/asio_forward_decl.h"


namespace common {

class AsioConnection_IF : public common::Interface {
public:
    virtual fwd::tcp_socket & getSocket() = 0;
    virtual void run() = 0;
    virtual void close() = 0;
};

} // namespace common


#endif // ECHO_CONNECTION_H
