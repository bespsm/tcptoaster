#include "include/asio_network_service.h"
#include "include/base_service.h"
#include "include/service_manager.h"
#include "include/asio_service.h"
#include <boost/asio.hpp>
#include "asio_forward_decl.h"
#include <iostream>

namespace {

class AsioNetworkService:
    public common::AsioNetworkService_IF{
private:
    common::AsioService_IF::SharedPtr asioService_;
    fwd::tcp_acceptor *acceptor_;
    fwd::tcp_endpoint endpoint_;
    std::string param_port;
    std::string param_address;

public:
virtual void initialize(common::ServiceManager_IF& manager, common::ServiceParameters& parameters) override{
    param_port = common::searchConfigGroupParameter(parameters,"endpoint","port","8005");
    param_address = common::searchConfigGroupParameter(parameters,"endpoint","address","127.0.0.1");
    asioService_ = manager.getService<common::AsioService_IF>(common::AsioServiceId);
    acceptor_ = new fwd::tcp_acceptor(asioService_->getIoService());
    //boost::asio::socket_base::enable_connection_aborted option(true);
    //acceptor_->set_option(option);
    boost::asio::ip::tcp::resolver resolver(asioService_->getIoService());
    endpoint_ = *resolver.resolve({param_address, param_port});
}

virtual void stop() override{
    //acceptor_->cancel();
    acceptor_->close();
    delete acceptor_;
    asioService_.reset();
}

fwd::tcp_acceptor &getAcceptor() override{
    return *acceptor_;
}

fwd::tcp_endpoint &getEndpoint() override{
    return endpoint_;
}

};

} // namespace

namespace common {

const ServiceId AsioNetworkServiceId = "asio_network";

BaseService_IF::UniquePtr AsioNetworkFactory::CreateService()
{
    return BaseService_IF::UniquePtr(new AsioNetworkService);
}
}

