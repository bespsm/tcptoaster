#ifndef INCLUDE_ASIO_NETWORK_SERVICE_IF_H
#define INCLUDE_ASIO_NETWORK_SERVICE_IF_H

#include "include/class_pointers.h"
#include "include/base_service.h"

#include "include/asio_forward_decl.h"


namespace common {

typedef std::string ServiceId;
extern const ServiceId AsioNetworkServiceId;

class AsioNetworkService_IF: public common::BaseService_IF
{
public:
    DEFINE_CLASS_POINTERS(AsioNetworkService_IF);
    //virtual void initialize(ServiceManager_IF& manager, const ServiceParameters& parameters) = 0;
    //virtual void stop() = 0;
    virtual fwd::tcp_acceptor& getAcceptor() = 0;
    virtual fwd::tcp_endpoint& getEndpoint() = 0;
    //virtual void addConnection(echo::EchoConnection::SharedPtr& c) = 0;
    //virtual void removeConnection(echo::EchoConnection::SharedPtr& c) = 0;
};


class AsioNetworkFactory : public common::BaseServiceFactory_IF  {
public:
    virtual common::BaseService_IF::UniquePtr CreateService() override;
};

} // namespace common

#endif // INCLUDE_ASIO_NETWORK_SERVICE_IF_H
