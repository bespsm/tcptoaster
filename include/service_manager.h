#ifndef INCLUDE_SERVICE_MANAGER_IF_H
#define INCLUDE_SERVICE_MANAGER_IF_H

#include "include/class_pointers.h"
#include "include/interface.h"
#include "include/parameters_service.h"


namespace common {

class BaseServiceFactory_IF;
class BaseService_IF;

struct ServiceInfromation{
    ServiceId Id;
    std::vector<ServiceId> Dependencies;
    std::shared_ptr<BaseServiceFactory_IF> Factory;
    ServiceParameters Parameters;
};

typedef std::vector<ServiceInfromation> ServicesInfromation;


class ServiceManager_IF : private common::Interface
{
public:
    DEFINE_CLASS_POINTERS(ServiceManager_IF);
    virtual void addService(const common::ServiceInfromation& service) = 0;

    virtual std::shared_ptr<BaseService_IF> getService(const ServiceId serviceid) = 0;

    template <typename ServiceClass>
    typename std::shared_ptr<ServiceClass> getService(const ServiceId serviceidd);
    virtual void start() = 0;
    virtual void stop() = 0;
};

ServiceManager_IF::UniquePtr CreateServiceManager();

template <typename ServiceClass>
typename std::shared_ptr<ServiceClass> ServiceManager_IF::getService(const ServiceId serviceid)
{
  return std::dynamic_pointer_cast<ServiceClass>(getService(serviceid));
}


} //common

#endif /* INCLUDE_SERVICE_MANAGER_IF_H */
