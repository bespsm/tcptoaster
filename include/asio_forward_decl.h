#pragma once

namespace boost {
	namespace system {
		class error_code;
	} // Namespace system

	namespace asio {
		// io_service
		class io_service;

		// socket
		template <typename Protocol>
		class stream_socket_service;

		template <typename Protocol, typename StreamSocketService>
		class basic_stream_socket;

		template <typename Protocol>
		class datagram_socket_service;

		template <typename Protocol, typename DatagramSocketService>
		class basic_datagram_socket;

		// acceptor
		template <typename Protocol>
		class socket_acceptor_service;

		template <typename Protocol, typename SocketAcceptorService>
		class basic_socket_acceptor;

		namespace ip {
			// tcp
			class tcp;

			// udp
			class udp;

			// resolver
			template <typename InternetProtocol>
			class resolver_service;

			template <typename InternetProtocol, typename ResolverService>
			class basic_resolver;

			// endpoint
			template <typename InternetProtocol>
			class basic_endpoint;

		} // Namespace ip
	} // Namespace asio
} // Namespace boost

namespace fwd {
	using io_service = boost::asio::io_service;

	using tcp = boost::asio::ip::tcp;
	using tcp_acceptor = boost::asio::basic_socket_acceptor<tcp, boost::asio::socket_acceptor_service<tcp>>;
	using tcp_endpoint = boost::asio::ip::basic_endpoint<tcp>;
	using tcp_resolver = boost::asio::ip::basic_resolver<tcp, boost::asio::ip::resolver_service<tcp>>;
	using tcp_socket = boost::asio::basic_stream_socket<tcp, boost::asio::stream_socket_service<tcp>>;

	using udp = boost::asio::ip::udp;
	using udp_endpoint = boost::asio::ip::basic_endpoint<udp>;
	using udp_resolver = boost::asio::ip::basic_resolver<udp, boost::asio::ip::resolver_service<udp>>;
	using udp_socket = boost::asio::basic_datagram_socket<udp, boost::asio::datagram_socket_service<udp>>;
} // Namespace fwd
