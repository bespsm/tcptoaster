#include <gtest/gtest.h>
#include "echo/clnt/echoclnt_connection.h"
#include <chrono>
#include "echo/echo_types.h"

TEST(ConnectionsHandle, generatingEchoDataWithCRLF)
{
    char test[10];
    echo::client::generateEchoData(test,size_t(10));
    EXPECT_EQ(test[8],'\r');
    EXPECT_EQ(test[9],'\n');
    char test1[10];
    echo::client::generateEchoData(test1,size_t(10));
    EXPECT_NE(test,test1);
}


TEST(ConnectionsHandle, processTiming)
{
    echo::ConnectionStatistics stat_;
    stat_.echoStart = std::chrono::system_clock::now();
    stat_.echoEnd = stat_.echoStart + std::chrono::microseconds(100);
    echo::client::processTiming(stat_);
    stat_.echoEnd = stat_.echoStart + std::chrono::microseconds(200);
    echo::client::processTiming(stat_);
    stat_.echoEnd = stat_.echoStart + std::chrono::microseconds(300);
    echo::client::processTiming(stat_);
    std::chrono::duration<double,std::micro> test(600);
    EXPECT_EQ(stat_.sumEchoTime,test);
    test = std::chrono::duration<double,std::micro>(100);
    EXPECT_EQ(stat_.fastEchoTime,test);
    test = std::chrono::duration<double,std::micro>(300);
    EXPECT_EQ(stat_.slowEchoTime,test);
}
