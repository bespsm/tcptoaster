#include <gtest/gtest.h>
#include "echo/srv/echosrv_connection.h"
#include <boost/lockfree/queue.hpp>
#include "echo/echo_types.h"


TEST(ConnectionsHandle, deserealizeEchoInQueue)
{
    boost::lockfree::queue<echo::Request*> requestQueue(10);
    echo::Request *requestBuffer = new echo::Request;

    char * inputBuffer = "11\r11\r\n22";
    requestBuffer = echo::server::deserealizeEchoInQueue(
        inputBuffer,strlen(inputBuffer),requestQueue,requestBuffer);
    inputBuffer = "22\r\n333";
    requestBuffer = echo::server::deserealizeEchoInQueue(
        inputBuffer,strlen(inputBuffer),requestQueue,requestBuffer);

        echo::Request *test;
    EXPECT_EQ(requestBuffer->message,"333");
        requestQueue.pop(test);
    EXPECT_EQ(test->message,"11\r11");
        requestQueue.pop(test);
    EXPECT_EQ(test->message,"2222");
}
