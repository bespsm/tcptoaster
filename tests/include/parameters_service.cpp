#include <gtest/gtest.h>
#include <algorithm>
#include "include/parameters_service.h"


const char* TestConfigFile = "tests/test2.conf";
const char* TestConfigFile1 = "tests/test1.conf";
const char* TestConfigPath = "tests/";


TEST(ServicesConfiguration, ParsesConfigurationFile)
{
    common::Configuration config = common::parseConfiguration(TestConfigFile);
    ASSERT_EQ(config.Services.size(), size_t(1));

    ASSERT_EQ(config.Parameters.Parameters.size(), size_t(2));
    const std::vector<common::Parameter> standaloneParams = config.Parameters.Parameters;
    EXPECT_EQ(standaloneParams[0].Name, "param1");
    EXPECT_EQ(standaloneParams[0].Value, "value1");
    EXPECT_EQ(standaloneParams[1].Name, "param2");
    EXPECT_EQ(standaloneParams[1].Value, "value2");

    const std::vector<common::ParametersGroup> standaloneGroups = config.Parameters.Groups;
    ASSERT_EQ(standaloneGroups.size(), size_t(2));
    ASSERT_EQ(standaloneGroups[0].Name, "group1");
    const std::vector<common::Parameter> group1Params = standaloneGroups[0].Parameters;
    ASSERT_EQ(group1Params.size(), size_t(2));
    ASSERT_EQ(group1Params[0].Name, "param1");
    ASSERT_EQ(group1Params[0].Value, "value1");
    ASSERT_EQ(group1Params[1].Name, "param2");
    ASSERT_EQ(group1Params[1].Value, "value2");

    ASSERT_EQ(standaloneGroups[1].Name, "group2");
    const std::vector<common::Parameter> group2Params = standaloneGroups[1].Parameters;
    ASSERT_EQ(group2Params.size(), size_t(2));
    ASSERT_EQ(group2Params[0].Name, "param1");
    ASSERT_EQ(group2Params[0].Value, "value1");
    ASSERT_EQ(group2Params[1].Name, "param2");
    ASSERT_EQ(group2Params[1].Value, "value2");


    const common::ServiceConfiguration module = config.Services.front();
    ASSERT_EQ(module.Id, "child_module");
    ASSERT_EQ(module.Dependencies.size(), size_t(2));
    ASSERT_EQ(module.Dependencies[0], "parent_module1");
    ASSERT_EQ(module.Dependencies[1], "parent_module2");

    ASSERT_EQ(module.Parameters.Parameters.size(), size_t(4));
    ASSERT_EQ(module.Parameters.Parameters[0].Name, "application_name");
    ASSERT_EQ(module.Parameters.Parameters[0].Value, "test server");
    ASSERT_EQ(module.Parameters.Parameters[1].Name, "application_uri");
    ASSERT_EQ(module.Parameters.Parameters[1].Value, "test_uri");
    ASSERT_EQ(module.Parameters.Parameters[2].Name, "application_type");
    ASSERT_EQ(module.Parameters.Parameters[2].Value, "server");

    ASSERT_EQ(module.Parameters.Groups.size(), size_t(2));
    ASSERT_EQ(module.Parameters.Groups[0].Name, "user_token_policy");
    ASSERT_EQ(module.Parameters.Groups[0].Parameters.size(), size_t(3));
    ASSERT_EQ(module.Parameters.Groups[0].Parameters[0].Name, "id");
    ASSERT_EQ(module.Parameters.Groups[0].Parameters[0].Value, "anonymous");
    ASSERT_EQ(module.Parameters.Groups[0].Parameters[1].Name, "type");
    ASSERT_EQ(module.Parameters.Groups[0].Parameters[1].Value, "anonymous");
    ASSERT_EQ(module.Parameters.Groups[0].Parameters[2].Name, "uri");
    ASSERT_EQ(module.Parameters.Groups[0].Parameters[2].Value, "http://mytesturi.org");
}

TEST(ServicesConfiguration, ParsesConfigurationFileSecond)
{
    common::Configuration config1 = common::parseConfiguration(TestConfigFile1);
    ASSERT_EQ(config1.Parameters.Parameters.size(), size_t(1));
    const std::vector<common::Parameter> aloneParams = config1.Parameters.Parameters;
    EXPECT_EQ(aloneParams[0].Name, "param1test");
    EXPECT_EQ(aloneParams[0].Value, "v");

    const std::vector<common::ParametersGroup> separateGroups = config1.Parameters.Groups;
    EXPECT_EQ(separateGroups.size(), size_t(2));
    EXPECT_EQ(separateGroups[0].Name, "group1test");

    const std::vector<common::Parameter> separate1Params = separateGroups[0].Parameters;
    EXPECT_EQ(separate1Params[0].Name, "param1");
    EXPECT_EQ(separate1Params[0].Value, "val11");
    EXPECT_EQ(separate1Params[1].Name, "param2");
    EXPECT_EQ(separate1Params[1].Value, "32312");
}

TEST(ServicesConfiguration, SavesConfigurationFile)
{
    common::ServiceConfiguration addon;
    addon.Id = "test_addon";
    addon.Dependencies.push_back("id1");
    addon.Dependencies.push_back("id2");
    common::ParametersGroup group;
    group.Name = "group";
    group.Parameters.push_back(common::Parameter("parameter", "value"));
    addon.Parameters.Parameters.push_back(common::Parameter("parameter", "value"));
    addon.Parameters.Groups.push_back(group);

    common::saveConfiguration(common::ServicesConfiguration({addon}), "test_config.config");
    const common::Configuration& config = common::parseConfiguration("test_config.config");
    ASSERT_EQ(config.Services.size(), size_t(1));
    const common::ServiceConfiguration& module = config.Services[0];
    ASSERT_EQ(module.Id, "test_addon");
    ASSERT_EQ(module.Dependencies.size(), size_t(2));
    ASSERT_EQ(module.Dependencies[0], "id1");
    ASSERT_EQ(module.Dependencies[1], "id2");
    ASSERT_EQ(module.Parameters.Parameters.size(), size_t(1));
    ASSERT_EQ(module.Parameters.Parameters[0].Name, "parameter");
    ASSERT_EQ(module.Parameters.Parameters[0].Value, "value");
    ASSERT_EQ(module.Parameters.Groups.size(), size_t(1));
    ASSERT_EQ(module.Parameters.Groups[0].Name, "group");
    ASSERT_EQ(module.Parameters.Groups[0].Parameters.size(), size_t(1));
    ASSERT_EQ(module.Parameters.Groups[0].Parameters[0].Name, "parameter");
    ASSERT_EQ(module.Parameters.Groups[0].Parameters[0].Value, "value");
}

TEST(ServicesConfiguration, ParsesConfigurationFilesInDirectory)
{
    common::Configuration config = common::parseConfigurationDirectory(TestConfigPath);
    ASSERT_EQ(config.Services.size(), size_t(1));
    ASSERT_EQ(config.Services[0].Id, "child_module");

    std::vector<common::ParametersGroup> standaloneGroups = config.Parameters.Groups;
    ASSERT_EQ(standaloneGroups.size(), size_t(4));

    auto fileconf1 = std::find(standaloneGroups.begin(),
        standaloneGroups.end(),
        common::ParametersGroup("group1test"));
    ASSERT_NE(fileconf1,standaloneGroups.end());

    auto fileconf2 = std::find(standaloneGroups.begin(),
        standaloneGroups.end(),
        common::ParametersGroup("group1"));
    ASSERT_NE(fileconf2,standaloneGroups.end());
}

TEST(ServicesConfiguration, searchConfigParameters)
{
    common::Configuration config = common::parseConfiguration(TestConfigFile);
    common::ServiceConfiguration module = config.Services.front();
    common::ServiceParameters params = module.Parameters;
    std::string res = common::searchConfigGroupParameter(params,"endpoint","url","defaultURL");
    ASSERT_EQ(res,"localhost:111");
    res = common::searchConfigGroupParameter(params,"endpoint","urllll","defaultURL");
    ASSERT_EQ(res,"defaultURL");
    res = common::searchConfigParameter(params,"application_name","default_name");
    ASSERT_EQ(res,"test server");
    res = common::searchConfigParameter(params,"applicationname","default_name");
    ASSERT_EQ(res,"default_name");

}
