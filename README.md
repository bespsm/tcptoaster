# Simple way to toast your eth0 #

How many tcp connections can process your interface? How fast processing is?
With this simple utility you can toast(test) it.
Thre are two tools:

**echoserver - echo tcp server** 
(accepts new connections, processes them and shows statistics);

**echoclient - echo tcp client** 
(makes connections to a given endpoint(address:port), sends a random chunk of data, waits for the answer, shows statistics);

Tools configuration is in toaster.conf

There are two versions of utility. Old one is in "src" directory. New is in "echo" and "incude". However, CMakeLists.txt preconfigured for each of them.

Config file is "toaster.conf". Put it in as folder with compiled program. (tcptoaster_config.xml - for config file for old version).

Dependencies: chrono, threads, mutex, boost::asio.

Folder test include unit tests (GTest).