#include "include/service_manager.h"
#include "include/parameters_service.h"
#include "include/asio_network_service.h"
#include "include/asio_service.h"
#include "echo/echo_types.h"
#include <boost/filesystem.hpp>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <set>

namespace {

std::string get_date(void) {
    std::time_t res;
    res = std::time(nullptr);
    std::tm* tm_result = std::localtime(&res);
    char buff[70];
    size_t size = std::strftime(buff, sizeof(buff), "%Y%m%d", tm_result);
    std::string date(buff, size);
    return date;
}

std::string get_time(void) {
    std::time_t res;
    res = std::time(nullptr);
    std::tm* tm_result = std::localtime(&res);
    char buff[70];
    size_t size = std::strftime(buff, sizeof(buff), "%H%M%S", tm_result);
    std::string time(buff, size);
    return time;
}

int get_success_sessions(std::vector<echo::ConnectionStatistics>& statistic){
    return std::count_if(statistic.begin(),statistic.end(),[](const echo::ConnectionStatistics & c){
        return (!c.readErrorsNum && !c.writeErrorsNum);
    });
}

int get_write_size(std::vector<echo::ConnectionStatistics>& statistic){
    int size = 0;
    for(echo::ConnectionStatistics &a: statistic){
        size+= a.bytesReaded;
    }
    return (size/1000000);
}

int get_read_size(std::vector<echo::ConnectionStatistics>& statistic){
    int size = 0;
    for(echo::ConnectionStatistics &a: statistic){
        size+= a.bytesReaded;
    }
    return (size/1000000);
}

int get_message_read(std::vector<echo::ConnectionStatistics>& statistic){
    int size = 0;
    for(echo::ConnectionStatistics &a: statistic){
        size+= a.messgeReadNum;
    }
    return size;
}

int get_message_write(std::vector<echo::ConnectionStatistics>& statistic){
    int size = 0;
    for(echo::ConnectionStatistics &a: statistic){
        size+= a.messgeWriteNum;
    }
    return size;
}

double get_conn_duration(std::vector<echo::ConnectionStatistics>& statistic){
    echo::timepoint conn_start = std::chrono::system_clock::now();
    echo::timepoint conn_end;
    for(echo::ConnectionStatistics &a: statistic){
        if (a.connectionStart < conn_start) conn_start = a.connectionStart;
        if (a.connectionEnd > conn_end) conn_end = a.connectionEnd;
    }
    std::chrono::duration<double, std::micro> dur = conn_end - conn_start;
    return dur.count();
}

double get_slowest_duration(std::vector<echo::ConnectionStatistics>& statistic){
    std::chrono::duration<double, std::micro> dur{100};
    for(echo::ConnectionStatistics &a: statistic){
        if (a.slowEchoTime > dur) dur = a.slowEchoTime;
    }
    return dur.count();
}

double get_medium_duration(std::vector<echo::ConnectionStatistics>& statistic){
    std::chrono::duration<double, std::micro> dur;
    long long messNum;
    for(echo::ConnectionStatistics &a: statistic){
        dur += a.sumEchoTime;
        messNum+=a.messgeReadNum;
    }
    return (dur.count()/messNum);
}

double get_fastest_duration(std::vector<echo::ConnectionStatistics>& statistic){
    std::chrono::duration<double, std::micro> dur{1000};
    for(echo::ConnectionStatistics &a: statistic){
        if (a.fastEchoTime < dur) dur = a.fastEchoTime;
    }
    return dur.count();
}


void get_connection_statistics(std::ostream &stream, std::vector<echo::ConnectionStatistics>& statistic){
    stream << "id;conn time(mcsec);Mbytes readed;"
           << "Mbytes writed;errors read;errors write;"
           << "messages readed;messages writed; "
           << "slowest echo(mcsec);medium echo;fastest echo;isconn\n";
    int id;
    std::chrono::duration<double, std::micro> dur;
    std::chrono::duration<double, std::micro> medium_echo;
    for(echo::ConnectionStatistics &a: statistic){
        dur = a.connectionEnd - a.connectionStart;
        medium_echo = (a.sumEchoTime / a.messgeWriteNum);
        stream << ++id << " ; " << dur.count() << " ; " << (a.bytesReaded/1000000) << " ; "
               << (a.bytesWrited/1000000)  << " ; " << a.readErrorsNum  << " ; "
               << a.writeErrorsNum << " ; " << a.messgeReadNum << " ; "
               << a.messgeWriteNum << " ; " << a.slowEchoTime.count() << " ; "
               << medium_echo.count() << " ; " << a.fastEchoTime.count() << " ; " << a.isOpen << '\n';
}
}



std::multiset<double> agregateEchoTime(std::vector<echo::ConnectionStatistics>& statistic){
    std::multiset<double> echoTimes;
    for(echo::ConnectionStatistics& a : statistic){
        while (a.echoTimeQueue.size() != 0){
            echoTimes.insert(a.echoTimeQueue.front().count());
            a.echoTimeQueue.pop();
        }
    }
    return std::move(echoTimes);
}

double percentageInPeriod(std::multiset<double>& echoTimes, double mcsec){
    double num = 0;
    for(auto a : echoTimes){
        if (a < mcsec) ++num;
    }
    if (num == 0) return 0;
    double res = (num/echoTimes.size())*100;
    return res;
}

void makeReport(std::ostream &stream, std::vector<echo::ConnectionStatistics>& statistic)
{
    if (statistic.size() == 0) {
        stream << "there aren't no connections\n";
        return;
    }
    std::multiset<double> agregatedEcho = agregateEchoTime(statistic);

    stream << "statistic " << get_date() << " - " << get_time() << '\n'
        //<< "threads: " << threads_count  << '\n'
        << "connections: " << statistic.size()  << '\n'
        << "successful testet connections: "  << get_success_sessions(statistic) << '\n'
        << "data readed size: " << get_read_size(statistic) << " Mbytes\n"
        << "data writed size: " << get_write_size(statistic) << " Mbytes\n"
        << "messages readed: " << get_message_read(statistic) << '\n'
        << "messages writed: " << get_message_write(statistic) << '\n'
        << "all sessions connection duration: "
        << get_conn_duration(statistic) << " microsec\n"

        << "percentage of echo duration in 100 mcsec: "
        << percentageInPeriod(agregatedEcho, double(100)) << " %\n"
        << "percentage of echo duration in 50 mcsec: "
        << percentageInPeriod(agregatedEcho, double(50)) << " %\n"

        << "slowest echo duration: "
        << get_slowest_duration(statistic) << " mcsec\n"
        << "medium echo duration: "
        << get_medium_duration(statistic) << " mcsec\n"
        << "fastest echo duration: "
        << get_fastest_duration(statistic) << " mcsec\n\n";
   get_connection_statistics(stream,statistic);
}

}



namespace echo {

void AddParametersFromConfiguration(common::ServiceInfromation& info, common::Configuration& cofig){
    std::vector<common::ServiceConfiguration>::iterator iter =
    std::find_if(cofig.Services.begin(),cofig.Services.end(),
    [info](const common::ServiceConfiguration& serviceConfig){
       return info.Id == serviceConfig.Id ? true : false;
    });
    if (iter != cofig.Services.end()){
        info.Parameters = iter->Parameters;
    }
}

void RegisterServices(common::ServicesInfromation& servicesInfo, common::ServiceManager_IF &manager){
    for(auto info : servicesInfo){
        manager.addService(info);
    }
}

common::ServiceInfromation SpecifyAsioService(){
    common::ServiceInfromation asio;
    asio.Id = common::AsioServiceId;
    asio.Factory = std::make_shared<common::AsioFactory>();
    return asio;
}

common::ServiceInfromation SpecifyAsioNetworkService(){
    common::ServiceInfromation AsioNetwork;
    AsioNetwork.Id = common::AsioNetworkServiceId;
    AsioNetwork.Dependencies.push_back(common::AsioServiceId);
    AsioNetwork.Factory = std::make_shared<common::AsioNetworkFactory>();
    return AsioNetwork;
}

void makeFileReport(std::vector<ConnectionStatistics> statistic){
    if (!boost::filesystem::exists(boost::filesystem::path("logs")))
        boost::filesystem::create_directory(boost::filesystem::path("logs"));
    std::string log_name = "logs/logfile_";
    log_name.append(get_date());
    log_name.append(1,'_');
    log_name.append(get_time());
    log_name.append(std::string(".csv"));
    std::fstream fileReport(log_name, std::ios::out);
    makeReport(fileReport,statistic);
    fileReport.close();
    std::cout << "logged in " << log_name << '\n';
}

void makeConsoleReport(std::vector<ConnectionStatistics> statistic){
    makeReport(std::cout,statistic);
}


}
