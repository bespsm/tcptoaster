#include "echo/srv/echosrv_connection.h"
#include "include/service_manager.h"
#include "include/asio_service.h"
#include "include/asio_network_service.h"
#include "include/base_service.h"
#include "echo/echo_types.h"
//#include "echo/echo_manager_service.h"
#include "echo/srv/echoserv_manager_service.h"
#include <boost/asio.hpp>
#include <set>
#include <mutex>
#include <iostream>


namespace {

class EchoServerManagerService:
    public echo::EchoManagerService_IF{
private:
    std::set<echo::EchoConnection_IF::SharedPtr> Connections_;
    echo::EchoConnection_IF::SharedPtr conncetion_;
    common::AsioService_IF::SharedPtr asioService_;
    common::AsioNetworkService_IF::SharedPtr networkService_;
    echo::ConnectionParameters connectionParams_;
    std::mutex mutex_;

public:
virtual void initialize(common::ServiceManager_IF& manager, common::ServiceParameters& parameters) override{
    connectionParams_.buffer_size = std::stoi(common::searchConfigParameter(parameters,"buffer_size","4096"));
    connectionParams_.write_attempts = std::stoi(common::searchConfigParameter(parameters,"write_attempts","3"));
    connectionParams_.read_attempts = std::stoi(common::searchConfigParameter(parameters,"read_attempts","3"));
    connectionParams_.errors_timeout = std::stoi(common::searchConfigParameter(parameters,"errors_timeout","3"));
    asioService_ = manager.getService<common::AsioService_IF>(common::AsioServiceId);
    networkService_ = manager.getService<common::AsioNetworkService_IF>(common::AsioNetworkServiceId);
    //listen connection
    boost::asio::ip::tcp::acceptor& acceptor = networkService_->getAcceptor();
    boost::asio::ip::tcp::endpoint& endpoint = networkService_->getEndpoint();
    acceptor.open(endpoint.protocol());
    acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
    acceptor.bind(endpoint);
    acceptor.listen();
    acceptConnection();
}

void acceptConnection(){
    conncetion_.reset();
    conncetion_ = echo::server::CreateEchoServerConnection(asioService_->getIoService(),connectionParams_);
    networkService_->getAcceptor().async_accept(conncetion_->getSocket(),
    [this](boost::system::error_code ec){
    if (ec.value() == 125){
        return;
    }
    if (!ec){
        addConnection(conncetion_);
        conncetion_->run();
    }
    acceptConnection();
    });
}

virtual void stop() override{
    //conncetion_->close();
    //networkService_->getAcceptor().close();
    for (auto c: Connections_)
      c->close();
    std::lock_guard<std::mutex> lock(mutex_);
    Connections_.clear();
}


void addConnection(echo::EchoConnection_IF::SharedPtr c) override {
    std::lock_guard<std::mutex> lock(mutex_);
    Connections_.insert(c);
}

virtual std::vector<echo::ConnectionStatistics> getStatistics() override{
    std::vector<echo::ConnectionStatistics> stats;
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if (Connections_.size() == 0 ) return stats;
    }
    for(echo::EchoConnection_IF::SharedPtr a : Connections_){
        stats.push_back(a->getStatistics());
    }
    return stats;
}

};

} // namespace


namespace echo {
namespace server {

const common::ServiceId EchoServerManagerServiceID = "echoserver_manager";

common::BaseService_IF::UniquePtr EchoServerManagerFactory::CreateService(){
    return common::BaseService_IF::UniquePtr(new EchoServerManagerService);
}
}


} // namespace echo
