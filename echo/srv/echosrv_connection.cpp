#include "echo/srv/echosrv_connection.h"
#include "echo/echo_types.h"
#include <boost/lockfree/queue.hpp>
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

namespace {

class EchoServerConnection : public echo::EchoConnection_IF{

private:
    boost::asio::io_service &service_;
    echo::ConnectionParameters params_;
    boost::asio::ip::tcp::socket socket_;
    echo::ConnectionStatistics statistic_;
    boost::asio::deadline_timer timer_;
    boost::lockfree::queue<echo::Request*> requestQueue_;
    boost::lockfree::queue<echo::Reply*> replyQueue_;
    echo::Request* requestBuffer;
    enum { max_msg = 4096 };
    char readBuffer[max_msg], writeBuffer[max_msg];
public:

EchoServerConnection(
        boost::asio::io_service &service,
        const echo::ConnectionParameters& parameters):
    service_(service),
    params_(parameters),
    socket_(service)
    ,timer_(service,boost::posix_time::seconds(parameters.errors_timeout)),
    requestQueue_(10),
    replyQueue_(10),
    requestBuffer(new echo::Request)
{

}

const echo::ConnectionStatistics & getStatistics() override{
    return statistic_;
}

virtual boost::asio::ip::tcp::socket& getSocket(){
   return socket_;
}

void run() override{
    std::cout << "run coonection" << '\n';
    statistic_.isOpen = true;
    readEcho(params_.buffer_size);
}

void readEcho(std::size_t bytesRead){
    if (bytesRead == 0) return;
    socket_.async_read_some(
    boost::asio::buffer(readBuffer,bytesRead),
    [this](const boost::system::error_code ec, size_t bytes){
    if (!ec){
        if (isRequestToClose()) {
            doClose();
            return;
        }
        requestBuffer = echo::server::deserealizeEchoInQueue(readBuffer,bytes,requestQueue_,requestBuffer);
        if (handleEcho()){
            statistic_.bytesReaded += bytes;
            statistic_.messgeReadNum++;
            size_t num = serealize();
            writeEcho(num);
            readEcho(params_.buffer_size);
        }
    }
    else{
        statistic_.readErrorsNum++;
        onReadError(ec);
    }
    });
}

bool isRequestToClose(){
    return strstr(readBuffer,"close") == NULL ? false : true;
}

bool handleEcho() {
    echo::Request* request;
    echo::Reply* reply;
    while (requestQueue_.pop(request)){
        reply = new echo::Reply;
        reply->message = request->message;
        while(!replyQueue_.push(reply)){}
        delete request;
    }
    return true;
}

void close() {
    if(socket_.is_open()){
        writeClose();
        doClose();
    }
}

void doClose(){
    socket_.cancel();
    std::cout << "close coonection" << '\n';
    socket_.close();
    statistic_.isOpen = false;
    while (requestQueue_.pop(requestBuffer)){
        delete requestBuffer;
    }
    echo::Reply * reply;
    while (replyQueue_.pop(reply)){
        delete reply;
    }
}

void writeClose(){
    char close[] = "close\r\n";
    socket_.send(boost::asio::buffer(close));
}

void writeEcho(size_t bytesWrite){
    if (bytesWrite == 0) return;
    async_write(
        socket_,
        (boost::asio::buffer(writeBuffer,bytesWrite)),
        [this](const boost::system::error_code ec, size_t bytes){
        if (!ec) {
            statistic_.bytesWrited += bytes;
            statistic_.messgeWriteNum++;
        }
        else {
            statistic_.writeErrorsNum++;
            onWriteError(ec);
        }
   });
}

void onReadError(const boost::system::error_code &ec){
    if (!socket_.is_open()) return;
    else if (params_.read_attempts-- == 0 ) doClose();
    else if (params_.errors_timeout == 0 ){
        std::cerr << "on read error: " << ec << '\n';
        readEcho(params_.buffer_size);
    }
    else{
        std::cerr << "on read error waiting " << params_.errors_timeout << " sec\n";
        timer_.async_wait([this](const boost::system::error_code &ec){
            readEcho(params_.buffer_size);
        });
    }
}

void onWriteError(const boost::system::error_code &ec){
    if (!socket_.is_open()) return;
    if (params_.write_attempts-- == 0) doClose();
    else if (params_.errors_timeout == 0 ){
        std::cerr << "on write error: " << ec << '\n';
        size_t num = serealize();
        writeEcho(num);
    }
    else{
        std::cerr << "on read error waiting " << params_.errors_timeout << " sec\n";
        timer_.async_wait([this](const boost::system::error_code &ec){
            size_t num = serealize();
            writeEcho(num);
        });
    }
}

std::size_t serealize(){
    echo::Reply* reply;
    if (replyQueue_.pop(reply) == false) return 0;
    strcpy(writeBuffer,reply->message.c_str());
    strcat(writeBuffer,"\r\n");
    delete reply;
    return strlen(writeBuffer);
}

};
}

namespace echo {
namespace server{

EchoConnection_IF::SharedPtr CreateEchoServerConnection(
        boost::asio::io_service &service,
        const echo::ConnectionParameters& parameters){
    return EchoConnection_IF::SharedPtr(new EchoServerConnection(service,parameters));
}

echo::Request* deserealizeEchoInQueue(
    char * inputBuffer,
    size_t bytes,
    boost::lockfree::queue<echo::Request*>& requestQueue,
    echo::Request* requestBuffer){

    if (bytes == 0) return requestBuffer;
    int border_pos = 0;
    for(size_t i = 0; i < bytes; i++){
       if(*(inputBuffer+border_pos+i) == '\r'  &&
          *(inputBuffer+border_pos+i+1) == '\n') {
            requestBuffer->message.append(inputBuffer+border_pos,(i-border_pos));
            while(!requestQueue.push(requestBuffer)){}
            requestBuffer = new echo::Request;
            border_pos = i+2;
       }
       else if(i+1 == bytes){
           requestBuffer->message.append((inputBuffer+border_pos),(i+1-border_pos));
           return requestBuffer;
       }
    }
    return requestBuffer;
}

}}
