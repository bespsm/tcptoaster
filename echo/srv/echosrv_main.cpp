#include "include/service_manager.h"
#include "include/parameters_service.h"
#include "include/asio_network_service.h"
#include "include/asio_service.h"
#include "echo/echo_specification.h"
#include "echo/srv/echoserv_manager_service.h"
#include "echo/echo_manager_service.h"
#include "echo/echo_types.h"
#include <iostream>

namespace {

common::ServiceInfromation SpecifyEchoServerManagerService(){
    common::ServiceInfromation EchoServerManager;
    EchoServerManager.Id = echo::server::EchoServerManagerServiceID;
    EchoServerManager.Dependencies.push_back(common::AsioServiceId);
    EchoServerManager.Dependencies.push_back(common::AsioNetworkServiceId);
    EchoServerManager.Factory = std::make_shared<echo::server::EchoServerManagerFactory>();
    return EchoServerManager;
}


void specifyEchoServer(const std::string directoryPath, common::ServiceManager_IF &manager){
    common::Configuration configuration = common::parseConfigurationDirectory(directoryPath);
    common::ServicesInfromation servicesInfo;
    servicesInfo.push_back(echo::SpecifyAsioService());
    servicesInfo.push_back(echo::SpecifyAsioNetworkService());
    servicesInfo.push_back(SpecifyEchoServerManagerService());
    for(common::ServiceInfromation& a: servicesInfo){
        echo::AddParametersFromConfiguration(a,configuration);
    }
    echo::RegisterServices(servicesInfo,manager);
}

void consoleReport(common::ServiceManager_IF &serviceManager){
    echo::EchoManagerService_IF::SharedPtr echoManager =
    serviceManager.getService<echo::EchoManagerService_IF>(echo::server::EchoServerManagerServiceID);
    echoManager->getStatistics();
    echo::makeConsoleReport(echoManager->getStatistics());
}

void fileReport(common::ServiceManager_IF &serviceManager){
    echo::EchoManagerService_IF::SharedPtr echoManager =
    serviceManager.getService<echo::EchoManagerService_IF>(echo::server::EchoServerManagerServiceID);
    echo::makeFileReport(echoManager->getStatistics());
}
}

int main(int argc, char* argv[]) {
    try{
        common::ServiceManager_IF::UniquePtr manager = common::CreateServiceManager();
        specifyEchoServer(".",*manager);
        while (true) {
            std::string msg;
            std::getline(std::cin, msg);
            if (msg == "start")
                manager->start();
            else if (msg == "stop")
                manager->stop();
            else if (msg == "logfile")
                fileReport(*manager);
            else if (msg == "logconsole")
                consoleReport(*manager);
            else if (msg == "close" || msg == "exit"){
                return 0;
            }
            else
                std::cout << "error cmd: there are only: start,stop,logfile,logconsole,close\n";
        }
    }
    catch (const std::exception& exc){
      std::cout << exc.what() << std::endl;
    }
    catch (...){
      std::cout << "Unknown error." << std::endl;
    }
    return -1;
}
