#ifndef ECHO_SERVER_CONNECTION_H
#define ECHO_SERVER_CONNECTION_H

#include "echo/echo_connection.h"
#include "boost/lockfree/queue.hpp"

/*
namespace common {
template <typename T>
class threadsafe_queue;
}*/


namespace boost {/*
namespace lockfree {
template <typename T0,class T1,class T2,class T3,>
class queue;
}*/
namespace asio {
class io_service;
}}

namespace echo {

struct ConnectionParameters;

namespace server {

echo::EchoConnection_IF::SharedPtr CreateEchoServerConnection(
        boost::asio::io_service &service,
        const echo::ConnectionParameters& parameters);


echo::Request* deserealizeEchoInQueue(
    char * inputBuffer,
    size_t bytes,
    boost::lockfree::queue<echo::Request*>& requestQueue,
    echo::Request* requestBuffer);

}
}

#endif // ECHO_SERVER_CONNECTION_H
