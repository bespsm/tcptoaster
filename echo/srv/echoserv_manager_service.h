#ifndef ECHO_SERVER_MANAGER_SERVICE_H
#define ECHO_SERVER_MANAGER_SERVICE_H

#include "echo/echo_manager_service.h"



namespace echo {
namespace server {

extern const common::ServiceId EchoServerManagerServiceID;

class EchoServerManagerFactory : public common::BaseServiceFactory_IF  {
public:
    virtual common::BaseService_IF::UniquePtr CreateService() override;
};

}}


#endif // ECHO_SERVER_MANAGER_SERVICE_H
