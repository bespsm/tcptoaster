#ifndef ECHO_SPECIFICATION_H
#define ECHO_SPECIFICATION_H

#include "include/service_manager.h"

namespace echo {

struct ConnectionStatistics;

common::ServiceInfromation SpecifyAsioService();
common::ServiceInfromation SpecifyAsioNetworkService();
void RegisterServices(common::ServicesInfromation& servicesInfo, common::ServiceManager_IF &manager);
void AddParametersFromConfiguration(common::ServiceInfromation& info, common::Configuration& cofig);

void makeConsoleReport(std::vector<ConnectionStatistics> statistic);
void makeFileReport(std::vector<ConnectionStatistics> statistic);

} //echo

#endif /* ECHO_SPECIFICATION_H */
