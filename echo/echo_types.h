#ifndef ECHO_TYPES_H
#define ECHO_TYPES_H

#include <string>
#include <chrono>
#include <queue>

namespace echo {


struct Request{
    std::string message;
};

struct Reply{
    std::string message;
};

typedef std::chrono::time_point<std::chrono::system_clock> timepoint;

struct ConnectionStatistics{
    timepoint connectionStart;
    timepoint connectionEnd;
    timepoint echoStart;
    timepoint echoEnd;
    long bytesReaded = 0;
    long bytesWrited = 0;
    unsigned readErrorsNum = 0;
    unsigned writeErrorsNum = 0;
    long messgeReadNum = 0;
    long messgeWriteNum = 0;
    std::chrono::duration<double,std::micro> slowEchoTime{1};
    std::chrono::duration<double,std::micro> fastEchoTime{1000000};
    std::chrono::duration<double,std::micro> sumEchoTime{0};
    bool isOpen = false;
    std::queue<std::chrono::duration<double,std::micro>> echoTimeQueue;
};

struct ConnectionParameters{
    size_t buffer_size;
    int write_attempts;
    int read_attempts;
    long errors_timeout;
};



} //echo

#endif /* ECHO_TYPES_H */
