#ifndef ECHO_CONNECTION_H
#define ECHO_CONNECTION_H

#include "echo/echo_types.h"
#include "include/class_pointers.h"
#include "include/interface.h"
#include "include/asio_connection.h"
#include "include/asio_forward_decl.h"

namespace boost {
namespace asio {
class io_service;
}
}

namespace echo {

class EchoConnection_IF : public common::AsioConnection_IF {
public:
    DEFINE_CLASS_POINTERS(EchoConnection_IF);
    virtual const ConnectionStatistics & getStatistics() = 0;
    //virtual boost::asio::ip::tcp::socket& getSocket() = 0;
    //virtual void run() = 0;
    //virtual void close() = 0;
};



} // namespace echo


#endif // ECHO_CONNECTION_H
