#ifndef ECHO_MANAGER_SERVICE_H
#define ECHO_MANAGER_SERVICE_H

#include "include/base_service.h"


namespace echo {

class EchoConnection_IF;
struct ConnectionStatistics;

class EchoManagerService_IF: public common::BaseService_IF
{
public:
    DEFINE_CLASS_POINTERS(EchoManagerService_IF);
    //virtual void initialize(ServiceManager_IF& manager, const ServiceParameters& parameters) = 0;
    //virtual void stop() = 0;
    virtual std::vector<ConnectionStatistics> getStatistics() = 0;
    virtual void addConnection(std::shared_ptr<EchoConnection_IF> c) = 0;
};
}


#endif // ECHO_MANAGER_SERVICE_H
