#include "echo/clnt/echoclnt_connection.h"
#include "echo/echo_types.h"
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <chrono>

namespace {


class EchoClientConnection : public echo::EchoConnection_IF{

private:
    boost::asio::io_service &service_;
    echo::ConnectionParameters params_;
    boost::asio::ip::tcp::socket socket_;
    const fwd::tcp_endpoint& endpoint_;
    echo::ConnectionStatistics statistic_;
    boost::asio::deadline_timer timer_;
    enum { max_msg = 4096 };
    char readBuffer[max_msg], writeBuffer[max_msg];
public:

EchoClientConnection(
        boost::asio::io_service &service,
        const echo::ConnectionParameters& parameters,
        const fwd::tcp_endpoint& endpoint):
    service_(service),
    params_(parameters),
    socket_(service),
    endpoint_(endpoint),
    timer_(service)
{}

const echo::ConnectionStatistics & getStatistics() override{
    return statistic_;
}

virtual boost::asio::ip::tcp::socket& getSocket(){
   return socket_;
}

void run() override{
    statistic_.connectionStart = std::chrono::system_clock::now();
    socket_.async_connect(endpoint_,[this](const boost::system::error_code& er){
        if(!er){
            statistic_.connectionEnd = std::chrono::system_clock::now();
            std::cout << "run coonection" << '\n';
            statistic_.isOpen = true;
            writeEcho();
        }
        else{
            statistic_.connectionEnd = statistic_.connectionStart;
            doClose();
            std::cerr << "on connection error: " << er << '\n';
        }
    });
}

void writeEcho(){
    if(!socket_.is_open()) return;
    echo::client::generateEchoData(writeBuffer,params_.buffer_size);
    statistic_.echoStart = std::chrono::system_clock::now();
    async_write(
        socket_,
        (boost::asio::buffer(writeBuffer,params_.buffer_size)),
        [this](const boost::system::error_code ec, size_t bytes){
        if (!ec) {
            statistic_.bytesWrited += bytes;
            statistic_.messgeWriteNum++;
            readEcho();
        }
        else {
            statistic_.writeErrorsNum++;
            onWriteError(ec);
        }
   });
}

void readEcho(){
    socket_.async_read_some(
    boost::asio::buffer(readBuffer),
    [this](const boost::system::error_code ec, size_t bytes){
    if (!ec){
        statistic_.echoEnd = std::chrono::system_clock::now();
        if (isRequestToClose()) {
            doClose();
            return;
        }
        if (handleEcho()){
            statistic_.bytesReaded += bytes;
            statistic_.messgeReadNum++;
            echo::client::processTiming(statistic_);
        }
        writeEcho();
    }
    else {
        statistic_.readErrorsNum++;
        onReadError(ec);
    }});
}

bool isRequestToClose(){
    return strstr(readBuffer,"close") == NULL ? false : true;
}

bool handleEcho() {
    for(unsigned i;i < params_.buffer_size;i++){
        if (readBuffer[i] != writeBuffer[i]) return false;
    }
    return true;
}

void writeClose(){
    char close[] = "close\r\n";
    socket_.send(boost::asio::buffer(close));
}

void close() {
    if(socket_.is_open()){
        writeClose();
        doClose();
    }
}

void doClose(){
    socket_.cancel();
    std::cout << "close coonection" << '\n';
    socket_.close();
    statistic_.isOpen = false;
}

void onReadError(const boost::system::error_code &ec){
    if (!socket_.is_open()) return;
    else if (params_.read_attempts-- == 0 ) doClose();
    else if (params_.errors_timeout == 0 ){
        std::cerr << "on read error: " << ec << '\n';
        readEcho();
    }
    else{
        std::cerr << "on read error waiting " << params_.errors_timeout << " sec\n";
        timer_.expires_from_now(boost::posix_time::seconds(params_.errors_timeout));
        timer_.async_wait([this](const boost::system::error_code &ec){
            readEcho();
        });
    }
}

void onWriteError(const boost::system::error_code &ec){
    if (!socket_.is_open()) return;
    if (params_.write_attempts-- == 0) doClose();
    else if (params_.errors_timeout == 0 ){
        std::cerr << "on write error: " << ec << '\n';
        writeEcho();
    }
    else{
        std::cerr << "on write error waiting " << params_.errors_timeout << " sec\n";
        timer_.expires_from_now(boost::posix_time::seconds(params_.errors_timeout));
        timer_.async_wait([this](const boost::system::error_code &ec){
            writeEcho();
        });
    }
}

};
}

namespace echo {
namespace client{

EchoConnection_IF::SharedPtr CreateEchoClientConnection(
        boost::asio::io_service &service,
        const echo::ConnectionParameters& parameters,
        const fwd::tcp_endpoint& endpoint){
    return EchoConnection_IF::SharedPtr(new EchoClientConnection(service,parameters,endpoint));
}

void generateEchoData(char *data, size_t size){
    if (size == 0) return;
    const char crlf[] = "\r\n";
    size_t crlflen = strlen(crlf);
    for (unsigned i=0;i < size-crlflen;i++){
        data[i] = rand()%64 + 64;
    }
    strncpy((data+(size-crlflen)),crlf,crlflen);
}

void processTiming(ConnectionStatistics& stat){
    std::chrono::duration<double> duration = stat.echoEnd - stat.echoStart;
    stat.sumEchoTime += duration;
    if (stat.slowEchoTime < duration) stat.slowEchoTime = duration;
    if (stat.fastEchoTime > duration) stat.fastEchoTime = duration;

    while (stat.echoTimeQueue.size() > 19)
        stat.echoTimeQueue.pop();
    stat.echoTimeQueue.push(duration);
}
}}
