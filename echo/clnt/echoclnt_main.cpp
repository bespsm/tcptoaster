#include "include/service_manager.h"
#include "include/parameters_service.h"
#include "include/asio_network_service.h"
#include "include/asio_service.h"
#include "echo/echo_specification.h"
#include "echo/clnt/echoclnt_manager_service.h"
#include "echo/echo_manager_service.h"
#include "echo/echo_types.h"
#include <iostream>

namespace {

common::ServiceInfromation SpecifyEchoClientManagerService(){
    common::ServiceInfromation EchoClientManager;
    EchoClientManager.Id = echo::client::EchoClientManagerServiceID;
    EchoClientManager.Dependencies.push_back(common::AsioServiceId);
    EchoClientManager.Dependencies.push_back(common::AsioNetworkServiceId);
    EchoClientManager.Factory = std::make_shared<echo::client::EchoClientManagerFactory>();
    return EchoClientManager;
}

void specifyEchoClient(const std::string directoryPath, common::ServiceManager_IF &manager){
    //specification echo client
    common::Configuration configuration = common::parseConfigurationDirectory(directoryPath);
    common::ServicesInfromation servicesInfo;
    servicesInfo.push_back(echo::SpecifyAsioService());
    servicesInfo.push_back(echo::SpecifyAsioNetworkService());
    servicesInfo.push_back(SpecifyEchoClientManagerService());
    for(common::ServiceInfromation& a: servicesInfo){
        echo::AddParametersFromConfiguration(a,configuration);
    }
    echo::RegisterServices(servicesInfo,manager);
}

void consoleReport(common::ServiceManager_IF &serviceManager){
    echo::EchoManagerService_IF::SharedPtr echoManager =
    serviceManager.getService<echo::EchoManagerService_IF>(echo::client::EchoClientManagerServiceID);
    echoManager->getStatistics();
    echo::makeConsoleReport(echoManager->getStatistics());
}

void fileReport(common::ServiceManager_IF &serviceManager){
    echo::EchoManagerService_IF::SharedPtr echoManager =
    serviceManager.getService<echo::EchoManagerService_IF>(echo::client::EchoClientManagerServiceID);
    echo::makeFileReport(echoManager->getStatistics());
}
}


int main(int argc, char* argv[]) {
    try{
        common::ServiceManager_IF::UniquePtr manager = common::CreateServiceManager();
        specifyEchoClient(".",*manager);
        while (true) {
            std::string msg;
            std::getline(std::cin, msg);
            if (msg == "start")
                manager->start();
            else if (msg == "stop")
                manager->stop();
            else if (msg == "logfile")
                fileReport(*manager);
            else if (msg == "logconsole")
                consoleReport(*manager);
            else if (msg == "close" || msg == "exit"){
                return 0;
            }
            else
                std::cout << "error cmd: there are only: start,stop,logfile,logconsole,close\n";
        }
    }
    catch (const std::exception& exc){
      std::cout << exc.what() << std::endl;
    }
    catch (...){
      std::cout << "Unknown error." << std::endl;
    }
    return -1;
}
