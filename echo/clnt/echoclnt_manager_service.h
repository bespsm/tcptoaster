#ifndef ECHO_CLIENT_MANAGER_SERVICE_H
#define ECHO_CLIENT_MANAGER_SERVICE_H

#include "include/base_service.h"


namespace echo {
namespace client {

extern const common::ServiceId EchoClientManagerServiceID;

class EchoClientManagerFactory : public common::BaseServiceFactory_IF  {
public:
    virtual common::BaseService_IF::UniquePtr CreateService() override;
};

}}


#endif // ECHO_CLIENT_MANAGER_SERVICE_H
