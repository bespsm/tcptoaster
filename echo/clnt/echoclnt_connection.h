#ifndef ECHO_CLIENT_CONNECTION_H
#define ECHO_CLIENT_CONNECTION_H

#include "echo/echo_connection.h"
#include "include/asio_forward_decl.h"
//#include "echo/echo_types.h"



namespace boost {
namespace asio {
class io_service;
}
}

namespace echo {

struct ConnectionParameters;
struct ConnectionStatistics;

namespace client {


echo::EchoConnection_IF::SharedPtr CreateEchoClientConnection(
        boost::asio::io_service &service,
        const echo::ConnectionParameters& parameters,
        const fwd::tcp_endpoint& endpoint);


void generateEchoData(char *data, size_t size);
void processTiming(ConnectionStatistics& stat);

}
}


#endif // ECHO_CLIENT_CONNECTION_H
