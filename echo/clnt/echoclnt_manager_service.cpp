#include "echo/clnt/echoclnt_connection.h"
#include "include/service_manager.h"
#include "include/asio_service.h"
#include "include/asio_network_service.h"
#include "include/base_service.h"
#include "echo/echo_types.h"
#include "echo/clnt/echoclnt_manager_service.h"
#include "echo/echo_manager_service.h"
#include <boost/asio.hpp>
#include <set>
#include <mutex>
#include <iostream>

namespace {

class EchoClientManagerService:
    public echo::EchoManagerService_IF{
private:
    std::set<echo::EchoConnection_IF::SharedPtr> Connections_;
    echo::EchoConnection_IF::SharedPtr conncetion_;
    common::AsioService_IF::SharedPtr asioService_;
    common::AsioNetworkService_IF::SharedPtr networkService_;
    echo::ConnectionParameters connectionParams_;
    std::mutex mutex_;
    unsigned connections_num;

public:
virtual void initialize(common::ServiceManager_IF& manager, common::ServiceParameters& parameters) override{
    connectionParams_.buffer_size = std::stoi(common::searchConfigParameter(parameters,"buffer_size","512"));
    connectionParams_.write_attempts = std::stoi(common::searchConfigParameter(parameters,"write_attempts","3"));
    connectionParams_.read_attempts = std::stoi(common::searchConfigParameter(parameters,"read_attempts","3"));
    connectionParams_.errors_timeout = std::stoi(common::searchConfigParameter(parameters,"errors_timeout","3"));
    connections_num = std::stoi(common::searchConfigParameter(parameters,"connections_num","100"));

    asioService_ = manager.getService<common::AsioService_IF>(common::AsioServiceId);
    networkService_ = manager.getService<common::AsioNetworkService_IF>(common::AsioNetworkServiceId);
    addConnections();
}

void addConnections(){
    for (unsigned i = 0;i< connections_num;i++){
        conncetion_.reset();
        conncetion_ = echo::client::CreateEchoClientConnection(
            asioService_->getIoService(),
            connectionParams_,
            networkService_->getEndpoint());
        addConnection(conncetion_);
        conncetion_->run();
    }
}

virtual void stop() override{
    for (auto c: Connections_)
       c->close();
    std::lock_guard<std::mutex> lock(mutex_);
    Connections_.clear();
}


void addConnection(echo::EchoConnection_IF::SharedPtr c) override {
    std::lock_guard<std::mutex> lock(mutex_);
    Connections_.insert(c);
}

virtual std::vector<echo::ConnectionStatistics> getStatistics() override{
    std::vector<echo::ConnectionStatistics> stats;
    {
        std::lock_guard<std::mutex> lock(mutex_);
        if (Connections_.size() == 0 ) return stats;
    }
    for(echo::EchoConnection_IF::SharedPtr a : Connections_){
        stats.push_back(a->getStatistics());
    }
    return stats;
}

};

} // namespace


namespace echo {
namespace client {

const common::ServiceId EchoClientManagerServiceID = "echoclient_manager";

common::BaseService_IF::UniquePtr EchoClientManagerFactory::CreateService(){
    return common::BaseService_IF::UniquePtr(new EchoClientManagerService);
}

} // namespace client
} // namespace echo
